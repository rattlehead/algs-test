import java.util.Random;

public class SortUtil {

    private static Random random = new Random();

    public static <T extends Comparable<T>> boolean isSortedAsc(T[] array) {
        int arrayLength = array.length;
        for (int i = 0; i < arrayLength - 1; i++) {
            if (array[i].compareTo(array[i + 1]) > 0) {
                return false;
            }
        }
        return true;
    }

    public static void testAlg(SortAlg alg) {
        long accTime = 0;
        long startTime = 0;
        long endTime = 0;

        int numTests = 100;
        int arrayLength = 1000;

        for (int n = 0; n < numTests; n++) {
            Integer[] array = generateRandomIntegerArray(arrayLength);

            startTime = System.currentTimeMillis();
            alg.sort(array);
            endTime = System.currentTimeMillis();

            accTime += (endTime - startTime);
            if (!isSortedAsc(array)) {
                System.out.println("Error in the algorithm: " + alg.getName());
                return;
            }
        }

        System.out.println("Average running time for " + alg.getName() + " is " + (accTime / numTests)
                + " ms; total time is " + accTime + " ms");
    }

    private static Integer[] generateRandomIntegerArray(int arrayLength) {
        Integer[] array = new Integer[arrayLength];
        for (int i = 0; i < arrayLength; i++) {
            array[i] = random.nextInt();
        }

        return array;
    }
}

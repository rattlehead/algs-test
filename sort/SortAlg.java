import java.util.Arrays;

public abstract class SortAlg {

    public abstract <T extends Comparable<T>> void sort(T[] array);

    public abstract String getName();

    public static void printArray(Comparable[] array) {
        System.out.println(Arrays.toString(array));
    }

    protected void swap(Comparable[] array, int i, int j) {
        if (i == j) {
            return;
        }

        Comparable tmp = array[i];
        array[i] = array[j];
        array[j] = tmp;
    }
}

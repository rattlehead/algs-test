public class InsertionSort extends SortAlg {

    @Override
    public <T extends Comparable<T>> void sort(T[] array) {
        int arrayLength = array.length;
        for (int i = 1; i < arrayLength; i++) {
            for (int j = i; j > 0 && array[j].compareTo(array[j - 1]) < 0; j--) {
                swap(array, j, j - 1);
            }
        }
    }

    @Override
    public String getName() {
        return "Insertion Sort";
    }

    public static void main(String[] args) {
        SortAlg alg = new InsertionSort();
        SortUtil.testAlg(alg);
    }
}

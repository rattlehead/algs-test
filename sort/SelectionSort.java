public class SelectionSort extends SortAlg {

    @Override
    public <T extends Comparable<T>> void sort(T[] array) {
        int arrayLength = array.length;
        for (int i = 0; i < arrayLength - 1; i++) {
            T minEl = array[i];
            int minIndex = i;
            for (int j = i + 1; j < arrayLength; j++) {
                if (minEl.compareTo(array[j]) > 0) {
                    minEl = array[j];
                    minIndex = j;
                }
            }
            swap(array, i, minIndex);
        }
    }

    @Override
    public String getName() {
        return "Selection Sort";
    }

    public static void main(String[] args) {
        SortAlg alg = new SelectionSort();
        SortUtil.testAlg(alg);
    }
}
